require 'test_helper'

class AuctionListsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @auction_list = auction_lists(:one)
  end

  test "should get index" do
    get auction_lists_url
    assert_response :success
  end

  test "should get new" do
    get new_auction_list_url
    assert_response :success
  end

  test "should create auction_list" do
    assert_difference('AuctionList.count') do
      post auction_lists_url, params: { auction_list: { city: @auction_list.city, name: @auction_list.name } }
    end

    assert_redirected_to auction_list_url(AuctionList.last)
  end

  test "should show auction_list" do
    get auction_list_url(@auction_list)
    assert_response :success
  end

  test "should get edit" do
    get edit_auction_list_url(@auction_list)
    assert_response :success
  end

  test "should update auction_list" do
    patch auction_list_url(@auction_list), params: { auction_list: { city: @auction_list.city, name: @auction_list.name } }
    assert_redirected_to auction_list_url(@auction_list)
  end

  test "should destroy auction_list" do
    assert_difference('AuctionList.count', -1) do
      delete auction_list_url(@auction_list)
    end

    assert_redirected_to auction_lists_url
  end
end
