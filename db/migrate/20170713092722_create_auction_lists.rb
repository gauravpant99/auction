class CreateAuctionLists < ActiveRecord::Migration[5.0]
  def change
    create_table :auction_lists do |t|
      t.string :name
      t.string :city

      t.timestamps
    end
  end
end
