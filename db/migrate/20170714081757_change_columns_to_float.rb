class ChangeColumnsToFloat < ActiveRecord::Migration[5.0]
  def change
    change_column :auction_lists, :winning_bid, :float
    change_column :auction_lists, :seller_payout, :float
  end
end
