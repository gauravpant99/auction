class AddDetailsToAuctionList < ActiveRecord::Migration[5.0]
  def change
    add_column :auction_lists, :vehicle, :string
    add_column :auction_lists, :winning_bid, :integer
    add_column :auction_lists, :seller_payout, :integer
  end
end
