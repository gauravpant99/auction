# README

##Auction web installation/Deployment procedure:
-------------------------------------------------


#1. Demo app is running in AWS in the below link:
-------------------------------------------------

    http://35.164.93.217
    ![Auction1.JPG](https://bitbucket.org/repo/yp5gLn5/images/2371871702-Auction1.JPG)
    
    Upload Form Page:
    ![Auction2.JPG](https://bitbucket.org/repo/yp5gLn5/images/1283065143-Auction2.JPG)


#2. Rails and Gem Version
-------------------------------------------------
1.	Rails version             5.0.4 ##
2.	Ruby version              2.3.4-p301 (x86_64-linux-gnu) ##
3.	RubyGems version          2.5.2 ###
4.	Rack version              2.0.3 ##
5.	JavaScript Runtime        Node.js (V8) ##
6.	Environment               development
7.	Database adapter          sqlite3

#3. Deployment Steps:
-------------------------------------------------

1. Git clone 
   git clone https://gauravpant99@bitbucket.org/gauravpant99/auction.git
2. Install the necessary Gem :
__command__:
`bundle install` 
3. Migrate sqlite database using rake:
__command__:
`rake db:migrate`


#4. Install Apache 
-------------------------------------------------

1. sudo apt-get update
2. sudo apt-get install apache2
3. Create a virtual test file to run the rails server by copying the default file
   sudo cp /etc/apache2/sites-available/000-default.conf /etc/apache2/sites-available/testapp.conf
   
5. Open the config file: 
    sudo vi /etc/apache2/sites-available/testapp.conf
4. Edit or replace it on the server ServerName, ServerAlias, DocumentRoot
   and Directory.

```
#!apache

  <VirtualHost *:80>
     ServerName example.com
     ServerAlias www.example.com
     ServerAdmin webmaster@localhost
     DocumentRoot /home/rails/testapp/public
     RailsEnv development
     ErrorLog ${APACHE_LOG_DIR}/error.log
     CustomLog ${APACHE_LOG_DIR}/access.log combined
     <Directory "/home/rails/testapp/public">
         Options FollowSymLinks
         Require all granted
     </Directory>
  </VirtualHost>
```

#5. You can run the webrick server(default rails server) for quick test (No apache installation is required)
-----------------------------------------------------------------------------------------------------------

1. rails server
2. Go to the browser : http://localhost:3000
3. Import a csv fie in the below link
http://localhost:3000/upload
4. Sample Input file  should be given in csv format:
   Below is the sample input file to download:
    [Sample Input File](http://35.164.93.217/example_input.csv)
 
```
#!csv

  name,city,vehicle,winning_bid,seller_payout
  A13,Helen,Audi A4,16000,8889
  A13,Delhi,BMW 5,25000,5555
```