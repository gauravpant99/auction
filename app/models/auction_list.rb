class AuctionList < ApplicationRecord
   require 'csv'

   scope :ordered_by_created_at, -> { reorder(created_at: :asc) }

   private
   
   ## Import the uploaded file into the database 
   def self.import(file)
      csv_text = File.read(file.path)
      csv = CSV.parse(csv_text, :headers => true)
      csv.each do |row|
        AuctionList.create!(row.to_hash)
      end
   end

   ## Get total_profit, avg profit per vehicle for each of the auctions
   ## Only one query is triggered to calculate the output

   def self.get_auction_count 
     @auction_count= AuctionList.select("name as auction_name, count(name) as count, (sum(winning_bid)-sum(seller_payout)) as total_profit, (sum(winning_bid)-sum(seller_payout))/count(name) as avg_profit").group("name").ordered_by_created_at
   end

end
