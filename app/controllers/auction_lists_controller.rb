class AuctionListsController < ApplicationController
  before_action :set_auction_list, only: [:show, :edit, :update, :destroy]

  # GET /auction_lists
  # GET /auction_lists.json
  # get_auction_count method is defined in model 
  # in order to keep the controller skinny

  def index
    @auction_lists = AuctionList.all
    @auction_count = AuctionList.get_auction_count
  end


  ## GET /upload
  ## This method render the default upload form 
  ## for importing the auction data from csv file

  def upload_form
    render :upload_form
  end

  ## POST /import
  ## This action is invoked with the form submit
  ## of the upload form
  ## On success it will be redirected to show all the auctions
  
  ##This method will invoke the models method import 
  ## which will load the file into the database
  def import
     file = params[:file]
     ##If upload is not successful then render to the same page 
     ## with flash message
     begin
        AuctionList.import(file)
        redirect_to root_url, notice: "Successfully added the data"
     rescue => e
        flash.now[:notice] = "Can't import the file:  #{e.message}"
        render  :upload_form
     end
  end

  # GET /auction_lists/1
  # GET /auction_lists/1.json
  def show
  end

  # GET /auction_lists/new
  def new
    @auction_list = AuctionList.new
  end

  # GET /auction_lists/1/edit
  def edit
  end

  # POST /auction_lists
  # POST /auction_lists.json
  def create
    @auction_list = AuctionList.new(auction_list_params)

    respond_to do |format|
      if @auction_list.save
        format.html { redirect_to @auction_list, notice: 'Auction list was successfully created.' }
        format.json { render :show, status: :created, location: @auction_list }
      else
        format.html { render :new }
        format.json { render json: @auction_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /auction_lists/1
  # PATCH/PUT /auction_lists/1.json
  def update
    respond_to do |format|
      if @auction_list.update(auction_list_params)
        format.html { redirect_to @auction_list, notice: 'Auction list was successfully updated.' }
        format.json { render :show, status: :ok, location: @auction_list }
      else
        format.html { render :edit }
        format.json { render json: @auction_list.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /auction_lists/1
  # DELETE /auction_lists/1.json
  def destroy
    @auction_list.destroy
    respond_to do |format|
      format.html { redirect_to auction_lists_url, notice: 'Auction list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_auction_list
      @auction_list = AuctionList.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def auction_list_params
      params.require(:auction_list).permit(:name, :city, :vehicle, :winning_bid, :seller_payout)
    end
end
