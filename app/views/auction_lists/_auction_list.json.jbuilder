json.extract! auction_list, :id, :name, :city, :created_at, :updated_at
json.url auction_list_url(auction_list, format: :json)
