Rails.application.routes.draw do
  ##Below will generate the CRUD for auction_lists controller
  resources :auction_lists
  
  ##Routes for upload form 
  get 'upload', to: 'auction_lists#upload_form'

  ##Routes for post of the upload form
  post 'import', to: 'auction_lists#import'

  ##Routes for the root method
  root :to => "auction_lists#index"
end
